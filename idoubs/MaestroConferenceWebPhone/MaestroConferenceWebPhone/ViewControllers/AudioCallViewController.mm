//
//  AudioCallViewController.m
//  MaestroConferenceWebPhone
//
//  Created by Shahab Ejaz on 06/12/2016.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

#import "AudioCallViewController.h"

#undef TAG
#define kTAG @"AudioCallViewController///: "
#define TAG kTAG
#define kCallTimerSuicide	1.5f



@interface AudioCallViewController (Private) <UITextFieldDelegate>
- (void)onInviteEvent:(NSNotification *)notification;
- (void)updateViewAndState;
- (void)timerSuicideTick:(NSTimer*)timer;
@end

@implementation AudioCallViewController (Private)

- (void)onInviteEvent:(NSNotification*)notification {
    NgnInviteEventArgs* eargs = [notification object];
    
    if(!audioSession || audioSession.id != eargs.sessionId){
        return;
    }
    
    switch (eargs.eventType) {
        case INVITE_EVENT_INPROGRESS:
        case INVITE_EVENT_INCOMING:
        case INVITE_EVENT_RINGING:
        case INVITE_EVENT_LOCAL_HOLD_OK:
        case INVITE_EVENT_REMOTE_HOLD:
        default:
        {
            // updates view and state
            [self updateViewAndState];
            break;
        }
            
            // transilient events
        case INVITE_EVENT_MEDIA_UPDATING:
        {
            //self.labelStatus.text = @"Updating...";
            break;
        }
            
        case INVITE_EVENT_MEDIA_UPDATED:
        {
            //self.labelStatus.text = @"Updated";
            break;
        }
            
        case INVITE_EVENT_TERMINATED:
        case INVITE_EVENT_TERMWAIT:
        {
//             NgnAVSession* _audioSession = audioSession;

            // updates view and state
            [self updateViewAndState];
            // releases session
//            [NgnAVSession releaseSession:&_audioSession];
            // starts timer suicide
            [NSTimer scheduledTimerWithTimeInterval: kCallTimerSuicide
                                             target: self 
                                           selector: @selector(timerSuicideTick:) 
                                           userInfo: nil 
                                            repeats: NO];
            
            // workaround due to ARC
//            audioSession = _audioSession;
            break;
        }
    }
}

- (void)updateViewAndState {
    if(audioSession){
        switch (audioSession.state) {
            case INVITE_STATE_INPROGRESS:
            {
                NgnNSLog(TAG,@"Calling...");
                self.labelStatus.text = @"Calling...";
                
//                self.buttonAccept.hidden = YES;
                
//                self.buttonHideNumpad.hidden = !self->numpadIsVisible;
                
//                [self.buttonHangup setTitle:@"End" forState:kButtonStateAll];
//                self.buttonHangup.hidden = NO;
//                CGFloat pad = self.buttonHideNumpad.hidden ? self->bottomButtonsPadding : self->bottomButtonsPadding/2;
//                self.buttonHangup.frame = CGRectMake(self.buttonHangup.frame.origin.x,
//                                                     self.buttonHangup.frame.origin.y,
//                                                     self.buttonHideNumpad.hidden ?
//                                                     (self.viewBottom.frame.size.width - (2*pad)) : (self.viewBottom.frame.size.width/2) - (pad + pad/2),
//                                                     self.buttonHangup.frame.size.height);
                break;
            }
            case INVITE_STATE_INCOMING:
            {
                NSAssert(false, @"%@:%@, Incoming call is not handled", NSStringFromClass([self class]),NSStringFromSelector(_cmd));
//                self.labelStatus.text = @"Incoming call...";
//                
//                CGFloat pad = self->bottomButtonsPadding;
//                
//                self.numpadIsVisible = NO;
//                [self.buttonHangup setTitle:@"End" forState:kButtonStateAll];
//                self.buttonHangup.hidden = NO;
//                self.buttonHangup.frame = CGRectMake(pad/2,
//                                                     self.buttonHangup.frame.origin.y,
//                                                     self.viewBottom.frame.size.width/2 - pad,
//                                                     self.buttonHangup.frame.size.height);
//                
//                [self.buttonAccept setTitle:@"Accept" forState:kButtonStateAll];
//                self.buttonAccept.hidden = NO;
//                self.buttonAccept.frame = CGRectMake(pad/2 + self.buttonHangup.frame.size.width + pad/2,
//                                                     self.buttonAccept.frame.origin.y,
//                                                     self.buttonHangup.frame.size.width,
//                                                     self.buttonAccept.frame.size.height);
//                
//                self.imageSecure.hidden = ![audioSession isSecure];
//                
//                [[NgnEngine sharedInstance].soundService playRingTone];
                
                break;
            }
            case INVITE_STATE_REMOTE_RINGING:
            {
                 NSAssert(false, @"%@:%@, Remote ringing is not handled", NSStringFromClass([self class]),NSStringFromSelector(_cmd));
//                self.labelStatus.text = @"Remote is ringing";
                
//                self.buttonAccept.hidden = YES;
//                self.buttonHideNumpad.hidden = !self->numpadIsVisible;
//                
//                [self.buttonHangup setTitle:@"End" forState:kButtonStateAll];
//                self.buttonHangup.hidden = NO;
//                CGFloat pad = self.buttonHideNumpad.hidden ? self->bottomButtonsPadding : self->bottomButtonsPadding/2;
//                self.buttonHangup.frame = CGRectMake(self.buttonHangup.frame.origin.x,
//                                                     self.buttonHangup.frame.origin.y,
//                                                     self.buttonHideNumpad.hidden ?
//                                                     (self.viewBottom.frame.size.width - (2*pad)) : (self.viewBottom.frame.size.width/2) - (pad + pad/2),
//                                                     self.buttonHangup.frame.size.height);
//                
                [[NgnEngine sharedInstance].soundService playRingBackTone];
                break;
            }
            case INVITE_STATE_INCALL:
            {
                NgnNSLog(TAG,@"You are now connected %@",pinCode);

                self.labelStatus.text = @"You are now connected with";
                
//                self.buttonAccept.hidden = YES;
//                self.buttonHideNumpad.hidden = !self->numpadIsVisible;
//                
//                [self.buttonHangup setTitle:@"End" forState:kButtonStateAll];
//                self.buttonHangup.hidden = NO;
//                CGFloat pad = self.buttonHideNumpad.hidden ? self->bottomButtonsPadding : self->bottomButtonsPadding/2;
//                self.buttonHangup.frame = CGRectMake(self.buttonHangup.frame.origin.x,
//                                                     self.buttonHangup.frame.origin.y,
//                                                     self.buttonHideNumpad.hidden ?
//                                                     (self.viewBottom.frame.size.width - (2*pad)) : (self.viewBottom.frame.size.width/2) - (pad + pad/2),
//                                                     self.buttonHangup.frame.size.height);
                
//                self.imageSecure.hidden = ![audioSession isSecure];
                
                [[NgnEngine sharedInstance].soundService stopRingBackTone];
                [[NgnEngine sharedInstance].soundService stopRingTone];
                
//                self.imageSecure.hidden = ![audioSession isSecure];
                break;
            }
            case INVITE_STATE_TERMINATED:
            case INVITE_STATE_TERMINATING:
            {
                self.labelStatus.text = @"Terminating...";
                
//                self.buttonAccept.hidden = YES;
//                self.buttonHangup.hidden = YES;
//                self.buttonHideNumpad.hidden = YES;
                
                [[NgnEngine sharedInstance].soundService stopRingBackTone];
                [[NgnEngine sharedInstance].soundService stopRingTone];
                [self closeView];
                break;
            }
            default:
                NSAssert(false, @"%@:%@, default case is not handled", NSStringFromClass([self class]),NSStringFromSelector(_cmd));
                break;
        }
        
//        [AudioCallViewController applyGradienWithColors: [audioSession isSpeakerEnabled] ? kColorsBlue : nil
//                                                forView:self.buttonSpeaker withBorder:NO];
//        [AudioCallViewController applyGradienWithColors: [audioSession isLocalHeld] ? kColorsBlue : nil
//                                                forView:self.buttonHold withBorder:NO];
//        [AudioCallViewController applyGradienWithColors: [audioSession isMuted] ? kColorsBlue : nil
//                                                forView:self.buttonMute withBorder:NO];
    }
}

- (void)timerSuicideTick:(NSTimer*)timer {
    [self performSelectorOnMainThread:@selector(closeView) withObject:nil waitUntilDone:NO];
}



@end

@implementation AudioCallViewController

@synthesize labelRemoteParty;
@synthesize labelStatus;
@synthesize btnMute;
@synthesize btnSpeaker;
@synthesize btnKeyPad;
@synthesize btnHangup;
@synthesize textFieldDummy;

@synthesize pinCode;
@synthesize sessionId;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.labelRemoteParty.text = pinCode;
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(onInviteEvent:) name:kNgnInviteEventArgs_Name object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    audioSession = [NgnAVSession getSessionWithId: self.sessionId];
    if(audioSession){
        [[NgnEngine sharedInstance].soundService setSpeakerEnabled:[audioSession isSpeakerEnabled]];
        [self updateViewAndState];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNgnInviteEventArgs_Name object:nil];
    NgnNSLog(TAG, @"deallocated");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClick:(id)sender {
    if(audioSession) {
        if (sender == self.btnMute) {
            // Mute Btn pressed
            if([audioSession setMute:![audioSession isMuted]]){
                self.btnMute.selected = [audioSession isMuted];
            }
        } else if (sender == self.btnSpeaker) {
            // Speaker Btn pressed
            [audioSession setSpeakerEnabled:![audioSession isSpeakerEnabled]];
            if([[NgnEngine sharedInstance].soundService setSpeakerEnabled:[audioSession isSpeakerEnabled]]){
                self.btnSpeaker.selected = [audioSession isSpeakerEnabled];
            }
        } else if (sender == self.btnKeyPad) {
            // Keypad Btn pressed
            if (btnKeyPad.selected) {
                [self dismissKeyboard];
            } else {
                [self.textFieldDummy becomeFirstResponder];
                self.btnKeyPad.selected = YES;
            }
            
            
        } else if (sender == self.btnHangup) {
            // Hangup Btn pressed
            [audioSession hangUpCall];
            [self closeView];
        } else if (sender == self.btnLogo) {
            [self openMaestroWebsite];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (audioSession) {
        NSCharacterSet* digits = [NSCharacterSet decimalDigitCharacterSet];
        if ([string rangeOfCharacterFromSet:digits].location != NSNotFound) {
            [audioSession sendDTMF:[string intValue]];
            [[NgnEngine sharedInstance].soundService playDtmf:[string intValue]];
        }
    }
    return YES;
}


#pragma mark - overridden
- (void)dismissKeyboard {
    [self.textFieldDummy resignFirstResponder];
    self.btnKeyPad.selected = NO;
}

@end
