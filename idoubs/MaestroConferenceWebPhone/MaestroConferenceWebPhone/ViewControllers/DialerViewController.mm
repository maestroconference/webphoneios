//
//  DialerViewController.m
//  MaestroConferenceWebPhone
//
//  Created by Shahab Ejaz on 05/12/2016.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

#import "DialerViewController.h"
#import "AudioCallViewController.h"
#import "NumberPadAccessoryView.h"
#undef TAG
#define kTAG @"DialerViewController///: "
#define TAG kTAG

#define MAX_PIN_LENGTH 6
#define SAVED_PIN_KEY @"savedPin"

@interface DialerViewController ()
@property(nonatomic,strong) NumberPadAccessoryView  *numberPadDoneBtn;

@end
@interface DialerViewController (Private)

- (IBAction)buttonClicked:(id)sender;
- (void)updateStatus;
- (BOOL)validPin;
- (BOOL) makeAudioCallWithRemoteParty:(NSString *)remoteUri andSipStack:(NgnSipStack *)sipStack;
@end

@implementation DialerViewController (Private)


- (void)savePinValueChanged:(id)sender {
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    if (self.savePinSwitch.isOn) {
        [userdefaults setValue:self.pinTextField.text forKey:SAVED_PIN_KEY];
    } else {
        [userdefaults setValue:@"" forKey:SAVED_PIN_KEY];
    }
}

- (IBAction)buttonClicked:(id)sender {
    if (sender == self.btnCall) {
        [self call];
    } else if (sender == self.btnLogo) {
        [self openMaestroWebsite];
    } else  if (sender == self.savePinSwitch) {
        [self savePinValueChanged:sender];
    }
}

- (void)call {
    [self.pinTextField resignFirstResponder];
    //check if pin is valid
    if([self validPin]) {
        [self makeAudioCallWithRemoteParty:self.pinTextField.text andSipStack:[mSipService getSipStack]];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Provided PIN is not valid." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

- (void)updateStatus {
    if([mSipService isRegistered]){
        NgnNSLog(TAG,@"Connected");
    }
    else {
        NgnNSLog(TAG,@"Not Connected");
        [self closeView];

    }
}



- (BOOL)validPin {
    return (pinTextField.text.length == MAX_PIN_LENGTH);
}

- (BOOL) makeAudioCallWithRemoteParty: (NSString*) remoteUri andSipStack: (NgnSipStack*) sipStack{
    if(![NgnStringUtils isNullOrEmpty:remoteUri]){
        NgnAVSession* audioSession = [NgnAVSession makeAudioCallWithRemoteParty: [NSString stringWithFormat: @"sip:5555555556%@gateway3.wakeupu.net@%@",remoteUri, [mConfigurationService getStringWithKey:NETWORK_REALM]]
                                                                     andSipStack: [[NgnEngine sharedInstance].sipService getSipStack]];
        if(audioSession){
            AudioCallViewController *audioCallViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([AudioCallViewController class])];
            audioCallViewController.sessionId = audioSession.id;
            audioCallViewController.pinCode = remoteUri;
            [self presentViewController:audioCallViewController animated:NO completion:nil];
            return YES;
        }
    }
    return NO;
}


@end

@interface DialerViewController(SipCallbacks)
-(void) onRegistrationEvent:(NSNotification*)notification;
@end

@implementation DialerViewController (SipCallbacks)

//== REGISTER events == //
- (void)onRegistrationEvent:(NSNotification*)notification {
    NgnRegistrationEventArgs* eargs = [notification object];
    
    switch (eargs.eventType) {
            // provisional responses
        case REGISTRATION_INPROGRESS:
        case UNREGISTRATION_INPROGRESS:
            break;
            // final responses
        case REGISTRATION_OK:
        case REGISTRATION_NOK:
        {
            
        }
        case UNREGISTRATION_OK:
        case UNREGISTRATION_NOK:
        default:
            break;
    }
    [self updateStatus];
}
@end


@implementation DialerViewController
@synthesize pinTextField;
@synthesize savePinSwitch;
@synthesize btnCall;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.numberPadDoneBtn = [[NumberPadAccessoryView alloc] initWithTitle:@"CALL" delegate:self];
        self.pinTextField.inputAccessoryView = self.numberPadDoneBtn;
    }
    
    
    NgnEngine* ngnEngine = [NgnEngine sharedInstance];
    mSipService = [ngnEngine getSipService];
    mConfigurationService = [ngnEngine getConfigurationService];

    NSString *savedPin = [[NSUserDefaults standardUserDefaults] stringForKey:SAVED_PIN_KEY];
    [self.savePinSwitch setOn:(savedPin.length > 0 ? YES : NO)];
    self.pinTextField.text = savedPin;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateStatus];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(onRegistrationEvent:) name:kNgnRegistrationEventArgs_Name object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNgnRegistrationEventArgs_Name object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    NSLog(@"%@ deallocated",NSStringFromClass([self class]));
}


#pragma mark - overridden
- (void)dismissKeyboard {
    [pinTextField resignFirstResponder];
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    if(self.savePinSwitch.isOn) {
        [userdefaults setValue:self.pinTextField.text forKey:SAVED_PIN_KEY];
    }
}


@end


@interface DialerViewController (Delegate) <UITextFieldDelegate,NumberPadAccessoryViewDelegate>
@end

@implementation DialerViewController (Delegate)

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self.numberPadDoneBtn addObserver];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [self.numberPadDoneBtn removeObserver];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // empty string means backspace is pressed
    if([string isEqualToString:@""]) {
        return YES;
    } else {
        // check length if greater than the allowed length return false
        if (textField.text.length >= MAX_PIN_LENGTH && range.length == 0) {
            return NO; // return NO to not change text
        } else {
            // only allow Numeric characters.
            NSCharacterSet* digits = [NSCharacterSet decimalDigitCharacterSet];
            if ([string rangeOfCharacterFromSet:digits].location == NSNotFound) {
                return NO;
            }
            return YES;
        }
    }
}

- (void)accessoryViewIsTapped:(NumberPadAccessoryView *)view {
    [self call];
}


@end

