//
//  DialerViewController.h
//  MaestroConferenceWebPhone
//
//  Created by Shahab Ejaz on 05/12/2016.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

#import "CallViewController.h"

@interface DialerViewController : CallViewController {
    UITextField * pinTextField;
    UISwitch * savePinSwitch;
    UIButton * btnCall;

    NgnBaseService<INgnSipService>* mSipService;
    NgnBaseService<INgnConfigurationService>* mConfigurationService;
}
@property(nonatomic, retain) IBOutlet UITextField * pinTextField;
@property(nonatomic, retain) IBOutlet UISwitch * savePinSwitch;
@property(nonatomic, retain) IBOutlet UIButton * btnCall;

@end
