//
//  BaseViewController.h
//  MaestroConferenceWebPhone
//
//  Created by Shahab Ejaz on 08/12/2016.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@property(nonatomic,weak) IBOutlet UIButton *btnLogo;

- (void)openMaestroWebsite;

@end
