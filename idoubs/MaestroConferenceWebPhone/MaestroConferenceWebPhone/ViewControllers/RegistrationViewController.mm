//
//  RegistrationViewController.m
//  MaestroConferenceWebPhone
//
//  Created by Shahab Ejaz on 06/12/2016.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

#import "RegistrationViewController.h"
#import "DialerViewController.h"
#import "UIImage+animatedGIF.h"

@interface RegistrationViewController ()

@end

@interface RegistrationViewController (Private)
- (void)updateStatus;
- (IBAction)buttonClicked:(id)sender;
@end

@implementation RegistrationViewController (Private)
- (void)updateStatus {
    if([mSipService isRegistered]){
        NSLog(@"Connected");
        DialerViewController *dialerVC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([DialerViewController class])];
        [self presentViewController:dialerVC animated:NO completion:nil];
    }
    else {
        NSLog(@"Not Connected");
    }
}

- (IBAction)buttonClicked:(id)sender {
    if (sender == self.btnLogo) {
        [self openMaestroWebsite];
    }
}

@end

@interface RegistrationViewController (SipCallbacks)
-(void) onRegistrationEvent:(NSNotification*)notification;
@end

@implementation RegistrationViewController (SipCallbacks)

//== REGISTER events == //
- (void)onRegistrationEvent:(NSNotification*)notification {
    NgnRegistrationEventArgs* eargs = [notification object];
    
    switch (eargs.eventType) {
            // provisional responses
        case REGISTRATION_INPROGRESS:
        case UNREGISTRATION_INPROGRESS:
            break;
            // final responses
        case REGISTRATION_OK:
        case REGISTRATION_NOK:
        {
            NSLog(@"Unable to connect at the moment");
        }
        case UNREGISTRATION_OK:
        case UNREGISTRATION_NOK:
        default:
            break;
    }
    [self updateStatus];
}
@end



@implementation RegistrationViewController
@synthesize imageViewWifi;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *wifiImageURL = [[NSBundle mainBundle] URLForResource:@"bar-animation-3x" withExtension:@".gif"];
    UIImage *image = [UIImage animatedImageWithAnimatedGIFURL:wifiImageURL];
    self.imageViewWifi.animationImages = image.images;
    self.imageViewWifi.animationDuration = 1.6;

    self.imageViewWifi.image = image.images.lastObject;
    [self.imageViewWifi startAnimating];


    NgnEngine* ngnEngine = [NgnEngine sharedInstance];
    mSipService = [ngnEngine getSipService];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(onRegistrationEvent:) name:kNgnRegistrationEventArgs_Name object:nil];
    [self updateStatus];


}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self name:kNgnRegistrationEventArgs_Name object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    NSLog(@"%@ deallocated",NSStringFromClass([self class]));
}
@end
