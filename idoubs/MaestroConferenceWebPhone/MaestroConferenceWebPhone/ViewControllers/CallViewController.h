//
//  CallViewController.h
//  MaestroConferenceWebPhone
//
//  Created by Shahab Ejaz on 06/12/2016.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

#import "BaseViewController.h"
#import "iOSNgnStack.h"

@interface CallViewController : BaseViewController

- (void)closeView;
- (void)dismissKeyboard;
@end
