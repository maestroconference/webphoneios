//
//  RegistrationViewController.h
//  MaestroConferenceWebPhone
//
//  Created by Shahab Ejaz on 06/12/2016.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

#import "BaseViewController.h"
#import "iOSNgnStack.h"

@interface RegistrationViewController : BaseViewController {
    NgnBaseService<INgnSipService>* mSipService;
    UIImageView *imageViewWifi;
}

@property (retain, nonatomic) IBOutlet UIImageView *imageViewWifi;


@end
