//
//  AudioCallViewController.h
//  MaestroConferenceWebPhone
//
//  Created by Shahab Ejaz on 06/12/2016.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

#import "CallViewController.h"
@interface AudioCallViewController : CallViewController {
    
    UILabel *labelRemoteParty;
    UILabel *labelStatus;
    UIButton *btnMute;
    UIButton *btnSpeaker;
    UIButton *btnHangup;
    UIButton *btnKeyPad;
    UITextField *textFieldDummy;
    
    long sessionId;
    NgnAVSession* audioSession;
    NSString *pinCode;
}

@property (retain, nonatomic) IBOutlet UILabel *labelRemoteParty;
@property (retain, nonatomic) IBOutlet UILabel *labelStatus;
@property (retain, nonatomic) IBOutlet UIButton *btnMute;
@property (retain, nonatomic) IBOutlet UIButton *btnSpeaker;
@property (retain, nonatomic) IBOutlet UIButton *btnHangup;
@property (retain, nonatomic) IBOutlet UIButton *btnKeyPad;

@property (retain, nonatomic) IBOutlet UITextField *textFieldDummy;

@property (retain, nonatomic) NSString *pinCode;
@property (nonatomic) long sessionId;



@end
