//
//  CallViewController.m
//  MaestroConferenceWebPhone
//
//  Created by Shahab Ejaz on 06/12/2016.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

#import "CallViewController.h"
#undef TAG
#define kTAG @"CallViewController///: "
#define TAG kTAG


@interface CallViewController ()

@end

@implementation CallViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)closeView {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)dealloc{
    NgnNSLog(TAG,@" deallocated");
}

- (void)dismissKeyboard {
    NSAssert(false, @"%@, %@ should be overriden in subclass", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
