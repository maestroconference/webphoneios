//
//  AppDelegate.h
//  MaestroConferenceWebPhone
//
//  Created by Shahab Ejaz on 12/4/16.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    BOOL scheduleRegistration;
    BOOL multitaskingSupported;
}

@property (strong, nonatomic) UIWindow *window;


@end

