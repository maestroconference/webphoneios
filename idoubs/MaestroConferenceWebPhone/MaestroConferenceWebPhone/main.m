//
//  main.m
//  MaestroConferenceWebPhone
//
//  Created by Shahab Ejaz on 12/4/16.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
