#import "NumberPadAccessoryView.h"

@implementation NumberPadAccessoryView {
	
	NumberPadButton *doneBtn;

}

#pragma mark -
#pragma mark Initialization/Deallocation

- (id) initWithFrame:(CGRect)frame {
    CGFloat height = CGRectGetHeight([UIScreen mainScreen].bounds);
	self = [super initWithFrame:CGRectMake(0, 0, 1, 1)];
	if(!self)
        return self;
    
	return self;
}

- (id) initWithTitle:(NSString *)title delegate:(id)delegate {
    self = [self initWithFrame:CGRectMake(0, 0, 1, 1)];
    self.title = title;
    self.delegate = delegate;
    return self;
}


- (void) dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark -
#pragma mark View Layouting

- (void) keyboardWillShow:(NSNotification*)notification {
	//	Calc View Height
    NSTimeInterval keyboardAnimationDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    float btnHeight = CGRectGetHeight([notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue])/4;		// Four Rows

    if (btnHeight <= 0) {
        return;
    }
    
	float height = 0.0f; float width = 0.0f;
	if(UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
		width = CGRectGetWidth([UIScreen mainScreen].bounds);
		height = CGRectGetHeight([UIScreen mainScreen].bounds);
	} else {
        NSAssert(false,@"Only portrait mode is supported");
	}
    
    float btnWidth = (width/3)-2;	// Three Columns - 2px to account for lines
	
    
    NSLog(@"Keyboard height = %f",CGRectGetHeight([notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue]));
    NSLog(@"button height = %f",CGRectGetHeight([notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue])/4);
	
    if (doneBtn == nil) {
        doneBtn = [[NumberPadButton alloc] initWithFrame:CGRectMake(0, height, btnWidth, btnHeight)];
        doneBtn.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17.0f];
        
        [doneBtn setTitle:self.title forState:UIControlStateNormal];
        [doneBtn addTarget:self action:@selector(btnPressed) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:doneBtn];
        [self setNeedsLayout];
        [self layoutIfNeeded];
    }

    doneBtn.frame = CGRectMake(0, height, btnWidth, btnHeight);

    [UIView animateWithDuration:keyboardAnimationDuration/4
                          delay:keyboardAnimationDuration * 2/4
                        options:[[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue]
                     animations:^{
                         doneBtn.frame = CGRectMake(0, (height - btnHeight)+1, btnWidth, btnHeight);
                        
                     } completion:nil];
    
    /** Title color adaptation removed with custom Color
	//	Set Button Text Color to adapt to keyboard type..
    if([self findFirstResponderIn:nil].keyboardAppearance == UIKeyboardAppearanceDark){
		doneBtn.tintColor = [UIColor colorWithRed:97/255.0f green:97/255.0f blue:97/255.0f alpha:1.0f];
		[doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	} else {
		doneBtn.tintColor = [UIColor whiteColor];
		[doneBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	}
    */
    
    [doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneBtn setBackgroundColor:[UIColor colorWithRed:84.0/255.0 green:174.0/255.0 blue:32.0/255.0 alpha:1.0]];
    
    [[self currentWindow] addSubview:self];
    [[self currentWindow] bringSubviewToFront:self];
}

- (BOOL) pointInside:(CGPoint)point withEvent:(UIEvent *)event {
	if(CGRectContainsPoint(doneBtn.frame, point)){
		return true;
		//	Normally a touch wouldn't register on the UIButton, as it's not in frame/bounds. This fixes that.
	}
	
	return [super pointInside:point withEvent:event];
}

- (void)addObserver {
    //	Register for UIKeyboard frame changes...
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];

}
- (void)removeObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -
#pragma mark Responder Management/Retreival

- (UIView<UITextInputTraits>*) findFirstResponderIn:(UIView*)view {
	if(view == nil) view = [self currentWindow];
	
	UIView<UITextInputTraits> *firstResponder;
	
	for (UIView *iView in view.subviews){
		if([iView isFirstResponder]){
			return firstResponder = (UIView<UITextInputTraits>*)iView;
		} else if(!firstResponder && iView.subviews.count > 0){
			firstResponder = [self findFirstResponderIn:iView];
		}
	}
	
	return firstResponder;
}

- (UIWindow*) currentWindow {
	NSArray *windows = [UIApplication sharedApplication].windows;
    return [windows lastObject];
}

- (void)btnPressed {
    [[self findFirstResponderIn:nil] resignFirstResponder];
    if (_delegate &&  [_delegate respondsToSelector:@selector(accessoryViewIsTapped:)]) {
        [_delegate accessoryViewIsTapped:self];
    }
}

@end

@implementation NumberPadButton

//- (void) setHighlighted:(BOOL)highlighted {
//	if (highlighted){
//		self.backgroundColor = self.tintColor;
//	} else {
//		self.backgroundColor = [UIColor clearColor];
//	}
//}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
