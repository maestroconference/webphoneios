#import <UIKit/UIKit.h>

@class NumberPadAccessoryView;
@protocol NumberPadAccessoryViewDelegate <NSObject>
- (void)accessoryViewIsTapped:(NumberPadAccessoryView *)view;
@end

@interface NumberPadAccessoryView: UIView

@property(nonatomic, strong) NSString *title;
@property(nonatomic, weak) id<NumberPadAccessoryViewDelegate> delegate;
- (id)initWithTitle:(NSString *)title delegate:(id)delegate;
- (void)addObserver;
- (void)removeObserver;
@end

@interface NumberPadButton : UIButton
@end
