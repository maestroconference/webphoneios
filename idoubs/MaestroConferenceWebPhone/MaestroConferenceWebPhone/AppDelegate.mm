//
//  AppDelegate.m
//  MaestroConferenceWebPhone
//
//  Created by Shahab Ejaz on 12/4/16.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

#import "AppDelegate.h"
#import "UIView+Toast.h"

#import "iOSNgnStack.h"
#import "MediaContent.h"
#import "MediaSessionMgr.h"
#import "tsk_base64.h"

#undef TAG
#define kTAG @"MaestroConferenceAppDelegate///: "
#define TAG kTAG

#define kNotifKey									@"key"
#define kNotifKey_IncomingCall						@"icall"
#define kNotifIncomingCall_SessionId				@"sid"


#define kAlertMsgButtonOkText						@"OK"
#define kNetworkAlertMsgThreedGNotEnabled			@"Only 3G network is available. Please enable 3G and try again."
#define kNetworkAlertMsgNotReachable				@"No network connection"


@interface AppDelegate (Private)
-(void) networkAlert:(NSString*)message;
-(BOOL) queryConfigurationAndRegister;
-(void) setAudioInterrupt: (BOOL)interrupt;
@end

@implementation AppDelegate (Private)
-(void) networkAlert:(NSString*)message {
    if([UIApplication sharedApplication].applicationState == UIApplicationStateActive){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:kAlertMsgButtonOkText
                                              otherButtonTitles: nil];
        [alert show];
    }
}

- (void)showInternetNotFoundToast {
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageAlignment = NSTextAlignmentCenter;
    // present the toast with the new style and huuge time :)
    [self.window makeToast:kNetworkAlertMsgNotReachable
                duration:978307200.0
                position:CSToastPositionBottom
                   style:style];
}


- (void)hideToast {
    [self.window hideToasts];
}

-(BOOL) queryConfigurationAndRegister {
    BOOL on3G = ([NgnEngine sharedInstance].networkService.networkType & NetworkType_WWAN);
    BOOL use3G = [[NgnEngine sharedInstance].configurationService getBoolWithKey:NETWORK_USE_3G];
    if(on3G && !use3G){
        [self networkAlert:kNetworkAlertMsgThreedGNotEnabled];
        return NO;
    }
    else if(![[NgnEngine sharedInstance].networkService isReachable]){
        [self networkAlert:kNetworkAlertMsgNotReachable];
        return NO;
    }
    else {
        return [[NgnEngine sharedInstance].sipService registerIdentity];
    }
}

-(void) setAudioInterrupt: (BOOL)interrupt {
    NgnAVSession *avSession = [NgnAVSession getFirstActiveCallAndNot:-1];
    if (avSession) {
        [avSession setAudioInterrupt:interrupt];
    }
}


//
//	sip callback events implementation
//
@end
@interface AppDelegate(Sip_And_Network_Callbacks)
-(void) onNetworkEvent:(NSNotification*)notification;
-(void) onStackEvent:(NSNotification*)notification;
-(void) onRegistrationEvent:(NSNotification*)notification;
-(void) onInviteEvent:(NSNotification*)notification;
@end


@implementation AppDelegate(Sip_And_Network_Callbacks)

//== Network events == //
-(void) onNetworkEvent:(NSNotification*)notification {
    NgnNetworkEventArgs *eargs = [notification object];
    
    switch (eargs.eventType) {
        case NETWORK_EVENT_STATE_CHANGED:
        default:
        {
            NgnNSLog(TAG,@"NetworkEvent reachable=%@ networkType=%i",
                     [NgnEngine sharedInstance].networkService.reachable ? @"YES" : @"NO", [NgnEngine sharedInstance].networkService.networkType);
            
            if([NgnEngine sharedInstance].networkService.reachable) {
                [self hideToast];
                BOOL onMobileNework = ([NgnEngine sharedInstance].networkService.networkType & NetworkType_WWAN);
                
                if(onMobileNework){ // 3G, 4G, EDGE ...
                    MediaSessionMgr::defaultsSetBandwidthLevel(tmedia_bl_medium); // QCIF, SQCIF
                }
                else {// WiFi
                    MediaSessionMgr::defaultsSetBandwidthLevel(tmedia_bl_unrestricted);// SQCIF, QCIF, CIF ...
                }
                
                // unregister the application and schedule another registration
                BOOL on3G = onMobileNework; // Downgraded to 3G even if it could be 4G or EDGE
                BOOL use3G = [[NgnEngine sharedInstance].configurationService getBoolWithKey:NETWORK_USE_3G];
                if(on3G && !use3G){
                    [self networkAlert:kNetworkAlertMsgThreedGNotEnabled];
                    [[NgnEngine sharedInstance].sipService stopStackSynchronously];
                }
                else { // "on3G and use3G" or on WiFi
                    // stop stack => clean up all dialogs
                    [[NgnEngine sharedInstance].sipService stopStackSynchronously];
                    [[NgnEngine sharedInstance].sipService registerIdentity];
                }
            }
            else {
                [self showInternetNotFoundToast];
                if([NgnEngine sharedInstance].sipService.registered){
                    [[NgnEngine sharedInstance].sipService stopStackSynchronously];
                }
            }
            
            break;
        }
    }
}

-(void) onStackEvent:(NSNotification*)notification {
    NgnStackEventArgs * eargs = [notification object];
    switch (eargs.eventType) {
        case STACK_STATE_STARTING:
        {
            NgnNSLog(TAG,@"STACK_STATE_STARTING");
            // this is the only place where we can be sure that the audio system is up
            [[NgnEngine sharedInstance].soundService setSpeakerEnabled:YES];
            
            break;
        }
        case STACK_DISCONNECTED: // Network connection closed
        {
            NgnNSLog(TAG,@"STACK_DISCONNECTED");
            // Uncomment next line if you want to connect again
            // [self queryConfigurationAndRegister];
            break;
        }
        default:
            break;
    }
}

//== REGISTER events == //
-(void) onRegistrationEvent:(NSNotification*)notification {
    // gets the new registration state
    ConnectionState_t registrationState = [[NgnEngine sharedInstance].sipService getRegistrationState];
    switch (registrationState) {
        case CONN_STATE_NONE:
        case CONN_STATE_TERMINATED:
            if(scheduleRegistration){
                scheduleRegistration = FALSE;
                [self queryConfigurationAndRegister];
            }
            break;
            
        case CONN_STATE_CONNECTING:
        case CONN_STATE_TERMINATING:
        case CONN_STATE_CONNECTED:
        default:
            break;
    }
}

//== INVITE (audio/video, file transfer, chat, ...) events == //
-(void) onInviteEvent:(NSNotification*)notification {
    NgnInviteEventArgs* eargs = [notification object];
    
    switch (eargs.eventType) {
        case INVITE_EVENT_INCOMING:
        {
            NgnAVSession* incomingSession = [NgnAVSession getSessionWithId: eargs.sessionId];
            if (incomingSession && [UIApplication sharedApplication].applicationState ==  UIApplicationStateBackground) {
                UILocalNotification* localNotif = [[UILocalNotification alloc] init];
                if (localNotif){
                    bool _isVideoCall = isVideoType(incomingSession.mediaType);
                    NSString *remoteParty = incomingSession.historyEvent ? incomingSession.historyEvent.remotePartyDisplayName : [incomingSession getRemotePartyUri];
                    
                    NSString *stringAlert = [NSString stringWithFormat:@"Call from \n %@", remoteParty];
                    if (_isVideoCall)
                        stringAlert = [NSString stringWithFormat:@"Video call from \n %@", remoteParty];
                    
                    localNotif.alertBody = stringAlert;
                    localNotif.soundName = UILocalNotificationDefaultSoundName;
                    localNotif.applicationIconBadgeNumber = ++[UIApplication sharedApplication].applicationIconBadgeNumber;
                    localNotif.repeatInterval = 0;
                    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                              kNotifKey_IncomingCall, kNotifKey,
                                              [NSNumber numberWithLong:incomingSession.id], kNotifIncomingCall_SessionId,
                                              nil];
                    localNotif.userInfo = userInfo;
                    [[UIApplication sharedApplication]  presentLocalNotificationNow:localNotif];
                }
            }
            else if(incomingSession){
#warning This needs to be our view controller
//                [CallViewController receiveIncomingCall:incomingSession];
            }
            
            [NgnAVSession releaseSession:&incomingSession];
            break;
        }
            
        case INVITE_EVENT_MEDIA_UPDATED:
        {
            NgnAVSession* session = [NgnAVSession getSessionWithId:eargs.sessionId];
            if (session) {
#warning Need to look what this code is doing
//                UIViewController* modalViewController = self.tabBarController.presentedViewController;
//                if (!modalViewController) {
//                    modalViewController = self.tabBarController.presentingViewController;
//                }
//                BOOL hasVideo = isVideoType(session.mediaType);
//                BOOL hasAudio = isAudioType(session.mediaType);
//                BOOL openNewCallView = (hasVideo && modalViewController != self.videoCallController)
//                || ((hasAudio && ! hasVideo) && modalViewController != self.audioCallController);
//                if (openNewCallView) {
//                    // Dismiss previous and display(present) the new one
//                    // animation must be NO because we are calling dismiss then present
//                    [self.tabBarController dismissViewControllerAnimated:NO completion:^{
//                        if (session.connectionState == CONN_STATE_CONNECTING || session.connectionState == CONN_STATE_CONNECTED) {
//                            [CallViewController displayCall:session];
//                        }
//                    }];
//                }
//                [NgnAVSession releaseSession:&session];
            }
            break;
        }
            
        case INVITE_EVENT_TERMINATED:
        {
            if ([UIApplication sharedApplication].applicationState ==  UIApplicationStateBackground) {
                // call terminated while in background
                // if the application goes to background while in call then the keepAwake mechanism was not started
                if([NgnEngine sharedInstance].sipService.registered && ![NgnAVSession hasActiveSession]){
                    if([[NgnEngine sharedInstance].configurationService getBoolWithKey:NETWORK_USE_KEEPAWAKE]){
                        [[NgnEngine sharedInstance] startKeepAwake];
                    }
                }
            }
            break;
        }
            
        default:
        {
            break;
        }
    }
}

@end

@implementation AppDelegate

@synthesize window;

static UIBackgroundTaskIdentifier sBackgroundTask = UIBackgroundTaskInvalid;
static dispatch_block_t sExpirationHandler = nil;

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // add observers
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(onNetworkEvent:) name:kNgnNetworkEventArgs_Name object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(onStackEvent:) name:kNgnStackEventArgs_Name object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(onRegistrationEvent:) name:kNgnRegistrationEventArgs_Name object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(onInviteEvent:) name:kNgnInviteEventArgs_Name object:nil];
    
    
    // start the engine
    [[NgnEngine sharedInstance] start];
    
    // must be here (after AVAudioSesssionInitialize)
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 6.0) {
        [[NSNotificationCenter defaultCenter]
         addObserver:self selector:@selector(onAudioSessionInteruptionEvent:) name:AVAudioSessionInterruptionNotification object:[AVAudioSession sharedInstance]];
    }
#if __IPHONE_OS_VERSION_MIN_REQUIRED < 6000
    else {
        [[AVAudioSession sharedInstance] setDelegate:self]; // deprecated starting SDK6
    }
#endif
    
    // Try to register the default identity
    [self queryConfigurationAndRegister];
    
    
    // enable the speaker: for errors, ringtone, numpad, ...
    // shoud be done after the SipStack is initialized (thanks to tdav_init() which will initialize the audio system)
    [[NgnEngine sharedInstance].soundService setSpeakerEnabled:YES];
    
    multitaskingSupported = [[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)] && [[UIDevice currentDevice] isMultitaskingSupported];
    sBackgroundTask = UIBackgroundTaskInvalid;
    sExpirationHandler = ^{
        NgnNSLog(TAG, @"Background task completed");
        // keep awake
        if([[NgnEngine sharedInstance].sipService isRegistered]){
            if([[NgnEngine sharedInstance].configurationService getBoolWithKey:NETWORK_USE_KEEPAWAKE]){
                [[NgnEngine sharedInstance] startKeepAwake];
            }
        }
        [[UIApplication sharedApplication] endBackgroundTask:sBackgroundTask];
        sBackgroundTask = UIBackgroundTaskInvalid;
    };
    
    if(multitaskingSupported){
        NgnNSLog(TAG, @"Multitasking IS supported");
    }
    
    // Set media parameters if you want
    MediaSessionMgr::defaultsSetAudioGain(0, 0);
    // Set some codec priorities
    /*int prio = 0;
     SipStack::setCodecPriority(tdav_codec_id_g722, prio++);
     SipStack::setCodecPriority(tdav_codec_id_speex_wb, prio++);
     SipStack::setCodecPriority(tdav_codec_id_pcma, prio++);
     SipStack::setCodecPriority(tdav_codec_id_pcmu, prio++);
     SipStack::setCodecPriority(tdav_codec_id_h264_bp, prio++);
     SipStack::setCodecPriority(tdav_codec_id_h264_mp, prio++);
     SipStack::setCodecPriority(tdav_codec_id_vp8, prio++);*/
    //...etc etc etc
    
    // work around because notification is not raised if app is launched without internet
    if(![NgnEngine sharedInstance].networkService.reachable) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if(![NgnEngine sharedInstance].networkService.reachable) {
                [self showInternetNotFoundToast];
            }
            
        });
    }
    
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 40000
    if(multitaskingSupported){
        ConnectionState_t registrationState = [[NgnEngine sharedInstance].sipService getRegistrationState];
        if(registrationState == CONN_STATE_CONNECTING || registrationState == CONN_STATE_CONNECTED){
            NgnNSLog(TAG, @"applicationDidEnterBackground (Registered or Registering)");
            //if(registrationState == CONN_STATE_CONNECTING){
            // request for 10min to complete the work (registration, computation ...)
            sBackgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:sExpirationHandler];
            //}
            if(registrationState == CONN_STATE_CONNECTED){
                if([[NgnEngine sharedInstance].configurationService getBoolWithKey:NETWORK_USE_KEEPAWAKE]){
                    if(![NgnAVSession hasActiveSession]){
                        [[NgnEngine sharedInstance] startKeepAwake];
                    }
                }
            }
            
            [application setKeepAliveTimeout:600 handler: ^{
                NgnNSLog(TAG, @"applicationDidEnterBackground:: setKeepAliveTimeout:handler^");
            }];
        }
    }
#endif /* __IPHONE_OS_VERSION_MIN_REQUIRED */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
    ConnectionState_t registrationState = [[NgnEngine sharedInstance].sipService getRegistrationState];
    NgnNSLog(TAG, @"applicationWillEnterForeground and RegistrationState=%d, NetworkReachable=%s", registrationState, [NgnEngine sharedInstance].networkService.reachable ? "TRUE" : "FALSE");
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 40000
    // terminate background task
    if(sBackgroundTask != UIBackgroundTaskInvalid){
        [[UIApplication sharedApplication] endBackgroundTask:sBackgroundTask]; // Using shared instance will crash the application
        sBackgroundTask = UIBackgroundTaskInvalid;
    }
    // stop keepAwake
    [[NgnEngine sharedInstance] stopKeepAwake];
    
#endif /* __IPHONE_OS_VERSION_MIN_REQUIRED */
    
    if(registrationState != CONN_STATE_CONNECTED){
        [self queryConfigurationAndRegister];
    }
    
    // check native contacts changed while app was runnig on background
//    if(self->nativeABChangedWhileInBackground){
//        // trigger refresh
//        self->nativeABChangedWhileInBackground = NO;
//    }

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NgnNSLog(TAG, @"applicationWillResignActive");

}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NgnNSLog(TAG, @"applicationWillTerminate");
    [[NgnEngine sharedInstance] stop];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void) onAudioSessionInteruptionEvent:(NSNotification*)notif {
    const NSInteger iType = [[[notif userInfo] valueForKey:AVAudioSessionInterruptionTypeKey] intValue];
    NgnNSLog(TAG, @"onAudioSessionInteruptionEvent:%d", (int)iType);
    switch (iType) {
        case AVAudioSessionInterruptionTypeBegan:
        {
            [self setAudioInterrupt:YES];
            break;
        }
        case AVAudioSessionInterruptionTypeEnded:
        {
            [self setAudioInterrupt:NO];
            break;
        }
    }
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    NgnNSLog(TAG, @"applicationDidReceiveMemoryWarning");
    [[NgnEngine sharedInstance].contactService unload];
    [[NgnEngine sharedInstance].historyService clear];
    [[NgnEngine sharedInstance].storageService clearFavorites];
}
@end
