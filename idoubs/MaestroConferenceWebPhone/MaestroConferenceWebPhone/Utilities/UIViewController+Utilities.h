//
//  UIViewController+Utilities.h
//  MaestroConferenceWebPhone
//
//  Created by Shahab Ejaz on 08/12/2016.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Utilities)
- (void)openURL:(NSURL *)url;
- (void)openURL:(NSURL *)url delegate:(id)delegate;
@end
