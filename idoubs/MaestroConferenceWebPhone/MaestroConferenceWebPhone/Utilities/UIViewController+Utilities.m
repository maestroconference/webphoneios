//
//  UIViewController+Utilities.m
//  MaestroConferenceWebPhone
//
//  Created by Shahab Ejaz on 08/12/2016.
//  Copyright © 2016 Shahab Ejaz. All rights reserved.
//

#import "UIViewController+Utilities.h"
@import SafariServices;


@implementation UIViewController (Utilities)
- (void)openURL:(NSURL *)url {
    [self openURL:url delegate:nil];
}

- (void)openURL:(NSURL *)url delegate:(id)delegate {
    @try {
        if([SFSafariViewController class]) {
            SFSafariViewController *sVC = [[SFSafariViewController alloc] initWithURL:url];
            sVC.delegate				= delegate;
            [self presentViewController:sVC animated:YES completion:nil];
        } else {
            [[UIApplication sharedApplication] openURL:url];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception = %@",exception);
    }
}

@end
